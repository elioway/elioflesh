![](https://elioway.gitlab.io/elioflesh/elio-Flesh-logo.png)

> Fully stacked, **the elioWay**

# elioflesh ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

**elioflesh** groups client applications, written in a range of frameworks, **the elioWay**.

- [elioflesh Documentation](https://elioway.gitlab.io/elioflesh/)
- [elioflesh Quickstart](https://elioway.gitlab.io/elioflesh/quickstart.html)

## Installing

- [elioway Prerequisites](https://elioway.gitlab.io/installing.html)
- [Installing elioflesh](https://elioway.gitlab.io/elioflesh/installing.html)

# Credits

- [elioflesh Credits](https://elioway.gitlab.io/elioflesh/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioflesh/apple-touch-icon.png)
