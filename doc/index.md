> And the forms!<br> > _OMG_, the forms.<br> > _OYG_, the forms!<br>
> All the gods, WTF!<br> > <br>
> I don't want to write another form. I don't want to have to write "form.onSubmit" ever again. **Tim Bushell**

# Made Flesh

<aside>
  <dl>
  <dd>Sated at length, ere long I might perceive</dd>
  <dd>Strange alteration in me, to degree</dd>
  <dd>Of reason in my inward powers; and speech</dd>
  <dd>Wanted not long; though to this shape retained.</dd>
  <dd>Thenceforth to speculations high or deep</dd>
  <dd>I turned my thoughts, and with capacious mind</dd>
  <dd>Considered all things visible in Heaven,</dd>
  <dd>Or Earth, or Middle; all things fair and good</dd>
</dl>
</aside>

**elioflesh** provides the UX, handles templating, and is responsible for pulling the four **elioWay** layers of an app together.

**elioflesh** groups client applications, for <https://schema.org> _schemaTypes_, written in a range of frameworks, **the elioWay**.

**elioWay** is semantic. Apps should be focused on providing functionality suitable to the engaged thing's type because the user expectation for working with an `Action` is different to a `MapItem` or `Image`

[elioflesh Dogma](/elioflesh/dogma.html)

## Flesh revealed

Of all the layers in the **elioWay** stack, **elioflesh** is the least developed. Completion would mean this project hits the milestone of having a fully working prototype.

Below are some of the projects which are either early attempts to play with the idea, or strands needing some love and development.

<section>
  <figure>
  <a href="/elioflesh/sammy-flesh/artwork/splash.jpg" target="_splash"><img src="/elioflesh/sammy-flesh/artwork/splash.jpg" target="_splash"></a>
  <h3>Early prototype</h3>
  <p>A RESTful Evented Javascript client built on top of SammyJs (a tiny javascript framework built on top of jQuery), the <strong>elioWay</strong>.</p>
  <p><a href="/elioflesh/sammy-flesh/"><button><img src="/elioflesh/sammy-flesh/favicoff.png"><br>sammy-flesh</button></a></p>
</figure>
  <figure>
  <a href="/elioflesh/veux-flesh/artwork/splash.jpg" target="_splash"><img src="/elioflesh/veux-flesh/artwork/splash.jpg" target="_splash"></a>
  <h3>On Hold</h3>
  <p>VUE library with components built <strong>the elioWay</strong>.</p>
  <p><a href="/elioflesh/veux-flesh"><button><img src="/elioflesh/veux-flesh/favicoff.png"><br>veux-flesh</button></a></p>
<figure>
  <a href="/elioflesh/express-flesh/artwork/splash.jpg" target="_splash"><img src="/elioflesh/express-flesh/artwork/splash.jpg" target="_splash"></a>
  <h3>In progress</h3>
  <p>Javascript client built on top of expressjs wth handlebars, the <strong>elioWay</strong>.</p>
  <p><a href="/elioflesh/express-flesh/"><button><img src="/elioflesh/express-flesh/favicoff.png"><br>express-flesh</button></a></p>
</figure>
</section>

## Related

<article>
  <a href="/eliothing/">
  <img src="/eliothing/favicoff.png">
  <div>
  <h4>eliothing</h4>
  <p>Gets the Schema for your <strong>elioflesh</strong>, the <strong>elioWay</strong>.
  </p>
</div>
</a>
</article>

<article>
  <a href="/eliobones/">
  <img src="/eliobones/favicoff.png">
  <div>
  <h4>eliobones</h4>
  <p>RESTful APIs for your <strong>elioflesh</strong>, the <strong>elioWay</strong>.
  </p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/">
  <img src="/eliosin/favicoff.png">
  <div>
  <h4>eliosin</h4>
  <p>Classless, wireframing, css stylesheets designed to work with <strong>elioflesh</strong>, the <strong>elioWay</strong>.
  </p>
</div>
</a>
</article>
