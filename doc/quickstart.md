# Quickstart elioflesh

- [elioflesh Prerequisites](/elioflesh/prerequisites.html)
- [Installing elioflesh](/elioflesh/installing.html)

## Nutshell

- **elioflesh** clients consuming **eliobones** RESTful APIs and services.

## Creating a new elioflesh app

1. Create a new app folder `your/repo/elioway/elioflesh/flesh-inFramework`.
2. Choose a Rest API from [eliobones](/eliobones).
3. Build a Client App in the Framework of your choice.
4. Reuse Code from the [flesh](/elioflesh/flesh) app.
5. Get it working.
6. Test it.
7. Document it using [chisel](/elioangels/chisel).
8. Brand it using [generator-art](/elioangels/generator-art). Tell everyone about it.
9. Use it to do things **the elioWay**.
10. Push it to the <https://gitlab.com/elioflesh/> group using [twig](/elioangels/twig).
