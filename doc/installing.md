# Installing elioflesh

- [Prerequisites](/elioflesh/prerequisites.html)

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioflesh.git
cd elioflesh
git submodule init
git submodule update
```
