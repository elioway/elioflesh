# elioflesh Credits

## Artwork

- [Reclining_Nude](https://commons.wikimedia.org/wiki/File:Amedeo_Modigliani,_1916,_Reclining_Nude_%28Nu_couch%C3%A9%29,_oil_on_canvas,_65.5_x_87_cm,_Foundation_E.G._B%C3%BChrle.jpg)

## Apps to build

- <https://mithril.js.org/simple-application.html>
- <https://backbonejs.org/#Model-View-separation>
- <https://vuejs.org>
